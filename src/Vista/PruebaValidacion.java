/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class PruebaValidacion {
    
    public static void main(String[] args) {
        String num="-12";
        try {
            System.out.println(getNumero(num));
        } catch (Exception ex) {
            System.err.println("Error:"+ex.getMessage());
        }
        
    }
    
    private static int getNumero(String numero) throws Exception
    {
    
        
        int num=Integer.parseInt(numero);
        if(num<0)
            throw new Exception("Numero negativo");
        return num;
    }
}
